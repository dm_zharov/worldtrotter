//
//  WebViewController.swift
//  WorldTrotter
//
//  Created by Дмитрий Жаров on 07.04.17.
//  Copyright © 2017 Big Nerd Ranch. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {
    
    @IBOutlet var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let bigNerdRanch = URLRequest(url: URL(string: "https://www.bignerdranch.com/")!)
        webView.loadRequest(bigNerdRanch)
        
    }
    
}
